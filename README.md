# Article Storage Application 



## Prerequisites

- Java 11
- Gradle 7.x (7.5 or later) or 8.x
- PostgreSQL 10.x or higher

## Install

- `git clone git@gitlab.com:platonov.vlad14/crudarticle.git `
- `gradle clean build`

### PostgreSQL 

- `CREATE DATABASE asa;`
- `CREATE USER asauser WITH PASSWORD 'asapass';`
- `ALTER ROLE asauser SUPERUSER; `


# Run

- `./gradlew bootRun`
- `gradle bootRun`