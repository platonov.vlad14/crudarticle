package com.article.storage.service;

import com.article.storage.dto.ArticleDTO;
import com.article.storage.model.Article;

import java.util.List;

public interface ArticleService {
    List<ArticleDTO> getAllArticle();

    ArticleDTO createArticle(ArticleDTO articleDTO);

    ArticleDTO getArticleById(Long id);

    ArticleDTO updateArticle(ArticleDTO articleDTO, Long id);

    void deleteArticle(Long id);
}
