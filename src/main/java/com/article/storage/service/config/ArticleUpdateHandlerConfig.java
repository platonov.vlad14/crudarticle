package com.article.storage.service.config;

import com.article.storage.model.Category;
import com.article.storage.service.impl.handler.ArticleUpdateHandler;
import com.article.storage.service.impl.handler.CompositeArticleUpdateHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Set;

@Configuration
public class ArticleUpdateHandlerConfig {
    @Bean
    CompositeArticleUpdateHandler compositeArticleUpdateHandler(List<ArticleUpdateHandler> handlers) {
        return new CompositeArticleUpdateHandler(handlers);
    }

    @Bean
    ArticleUpdateHandler articleUpdateAuthorHandler() {
        return (articleDTO, article) -> {
            if (articleDTO.getAuthor() != null) {
                article.setAuthor(articleDTO.getAuthor());
            }
            return article;
        };
    }

    @Bean
    ArticleUpdateHandler articleUpdateContentHandler() {
        return (articleDTO, article) -> {
            if (articleDTO.getContent() != null) {
                article.setContent(articleDTO.getContent());
            }
            return article;
        };
    }

    @Bean
    ArticleUpdateHandler articleUpdateCommentsHandler() {
        return (articleDTO, article) -> {
            if (articleDTO.getComments() != null) {
                article.setComments(articleDTO.getComments());
            }
            return article;
        };
    }

    @Bean
    ArticleUpdateHandler articleUpdateCommercialPaymentsSumHandler() {
        return (articleDTO, article) -> {
            if (articleDTO.getCommercialPaymentsSum() != null) {
                article.setCommercialPaymentsSum(articleDTO.getCommercialPaymentsSum());
            }
            return article;
        };
    }

    @Bean
    ArticleUpdateHandler articleUpdateCategoriesHandler() {
        return (articleDTO, article) -> {
            if (articleDTO.getCategories() != null) {
                Set<Category> newCategories = article.getCategories();
                newCategories.addAll(articleDTO.getCategories());
                article.setCategories(newCategories);
            }
            return article;
        };
    }
}
