package com.article.storage.service.impl.handler;

import com.article.storage.dto.ArticleDTO;
import com.article.storage.model.Article;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class CompositeArticleUpdateHandler implements ArticleUpdateHandler {
    private final List<ArticleUpdateHandler> handlers;

    @Override
    public Article handle(ArticleDTO articleDTO, Article article) {
        for (ArticleUpdateHandler handler : handlers) {
            article = handler.handle(articleDTO, article);
        }

        return article;
    }
}
