package com.article.storage.service.impl.handler;

import com.article.storage.dto.ArticleDTO;
import com.article.storage.model.Article;

@FunctionalInterface
public interface ArticleUpdateHandler {
    Article handle(ArticleDTO articleDTO, Article article);
}
