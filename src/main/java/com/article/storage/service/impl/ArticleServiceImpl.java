package com.article.storage.service.impl;

import com.article.storage.dto.ArticleDTO;
import com.article.storage.exception.NoSuchArticleException;
import com.article.storage.mapper.ArticleMapper;
import com.article.storage.repository.ArticleRepository;
import com.article.storage.model.Article;
import com.article.storage.service.ArticleService;
import com.article.storage.service.impl.handler.ArticleUpdateHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ArticleServiceImpl implements ArticleService {
    private final ArticleRepository repository;
    private final ArticleMapper mapper;

    @Qualifier("compositeArticleUpdateHandler")
    private final ArticleUpdateHandler updateHandler;

    @Override
    public List<ArticleDTO> getAllArticle() {
        List<Article> result = repository.findAll();
        log.info("IN getAll - {} articles found", result.size());
        return mapper.articlesToArticleDTOs(result);
    }

    @Override
    public ArticleDTO createArticle(ArticleDTO articleDTO) {
        Article article = mapper.articleDTOtoArticle(articleDTO);
        article.setCreatedOn(LocalDateTime.now());

        article = repository.save(article);
        log.info("Article created successfully. Article details: " + article);
        return mapper.articleToArticleDTO(article);
    }

    @Override
    public ArticleDTO getArticleById(Long id) {
        Article article = repository.findById(id).orElseThrow(() -> {
            throw new NoSuchArticleException(id);
        });

        log.info("IN findById - article: {} found by id", article);
        return mapper.articleToArticleDTO(article);
    }

    @Override
    @Transactional
    public ArticleDTO updateArticle(ArticleDTO articleDTO, Long id) {
        Optional<Article> result = repository.findById(id);
        if (result.isEmpty()) {
            throw new NoSuchArticleException(id);
        }

        Article article = updateHandler.handle(articleDTO, result.orElseThrow());


        article = repository.save(article);
        log.info("Article updated successfully. Article details:" + article);
        return mapper.articleToArticleDTO(article);
    }

    @Override
    @Transactional
    public void deleteArticle(Long id) {
        if (repository.findById(id).isPresent()) {
            repository.deleteById(id);
            log.info("IN delete - article with id: {} successfully deleted", id);
        }
    }
}
