package com.article.storage.mapper;

import com.article.storage.dto.ArticleDTO;
import com.article.storage.model.Article;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ArticleMapper {

    ArticleDTO articleToArticleDTO(Article article);

    Article articleDTOtoArticle(ArticleDTO article);

    List<ArticleDTO> articlesToArticleDTOs(List<Article> articles);

}
