package com.article.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticleStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArticleStorageApplication.class, args);
	}

}
