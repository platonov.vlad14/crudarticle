package com.article.storage.model;

public enum Category {
    SHOWBIZ,
    SPORT,
    EDUCATION
}
