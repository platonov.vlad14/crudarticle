package com.article.storage.exception;

public class NoSuchArticleException extends RuntimeException {

    private final Long id;

    public NoSuchArticleException(Long id) {
        super("no article found by id %s".formatted(id));
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
