package com.article.storage.rest;

import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.article.storage.config.H2TestProfileJPAConfig;
import com.article.storage.dto.ArticleDTO;
import com.article.storage.model.Category;
import com.article.storage.service.ArticleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(H2TestProfileJPAConfig.class)
@ActiveProfiles("testUsingH2")
@AutoConfigureMockMvc
class ArticleRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ArticleService articleService;

    private ArticleDTO articleDTO;
    private List<ArticleDTO> articlesDTOs;
    private ObjectMapper objectMapper;
    private ObjectNode objectNode;

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
        objectNode = objectMapper.createObjectNode();

        articleDTO = ArticleDTO.builder()
                .id(1L)
                .author("author")
                .content("content")
                .commercialPaymentsSum(BigDecimal.valueOf(100L))
                .comments("comments")
                .categories(Collections.singleton(Category.SHOWBIZ))
                .build();
        articleDTO = articleService.createArticle(articleDTO);
        articlesDTOs = Collections.singletonList(articleDTO);
    }

    @Test
    void shouldReturnAllArticles() throws Exception {
        mockMvc.perform(get("/api/v1/articles/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldCreateArticle() throws Exception {
        ArticleDTO articleDTO = ArticleDTO.builder()
                .id(2L)
                .author("Test Author")
                .content("Test Content")
                .commercialPaymentsSum(BigDecimal.valueOf(100L))
                .comments("Test Comments")
                .build();
        String jsonRequest = objectMapper.writeValueAsString(articleDTO);

        mockMvc.perform(post("/api/v1/articles/create")
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.author", is("Test Author")))
                .andExpect(jsonPath("$.content", is("Test Content")))
                .andExpect(jsonPath("$.commercialPaymentsSum", is(100)))
                .andExpect(jsonPath("$.comments", is("Test Comments")));

        List<ArticleDTO> articles = articleService.getAllArticle();
        assertEquals(2, articles.size());
    }

    @Test
    void shouldReturnArticleById() throws Exception {
        mockMvc.perform(get("/api/v1/articles/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.author", is("author")))
                .andExpect(jsonPath("$.content", is("content")));
    }

    @Test
    void shouldUpdateArticle() throws Exception {
        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setAuthor("Updated Title");
        articleDTO.setContent("Updated Content");
        String jsonRequest = objectMapper.writeValueAsString(articleDTO);

        mockMvc.perform(patch("/api/v1/articles/1")
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.author", is("Updated Title")))
                .andExpect(jsonPath("$.content", is("Updated Content")));

        ArticleDTO updatedArticle = articleService.getArticleById(1L);
        assertEquals("Updated Title", updatedArticle.getAuthor());
        assertEquals("Updated Content", updatedArticle.getContent());
    }

    @Test
    void shouldDeleteArticle() throws Exception {
        mockMvc.perform(delete("/api/v1/articles/1"))
                .andExpect(status().isNoContent());

        List<ArticleDTO> articles = articleService.getAllArticle();
        assertEquals(1, articles.size());
    }

    @Test
    void shouldNoCreateArticle() throws Exception {
        ArticleDTO articleDTO = ArticleDTO.builder()
                .id(3L)
                .commercialPaymentsSum(BigDecimal.valueOf(100L))
                .comments("Test Comments")
                .build();
        String jsonRequest = objectMapper.writeValueAsString(articleDTO);

        mockMvc.perform(post("/api/v1/articles/create")
                        .content(jsonRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                .andExpect(jsonPath("$.errors.author", is("Author is mandatory")))
                .andExpect(jsonPath("$.errors.content", is("Content is mandatory")));

        List<ArticleDTO> articles = articleService.getAllArticle();
        assertEquals(1, articles.size());
    }
}