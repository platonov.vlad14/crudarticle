package com.article.storage.mapper;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.article.storage.dto.ArticleDTO;
import com.article.storage.model.Article;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

public class ArticleMapperTest {
    private static final Long ARTICLE_ID = 1L;

    private ArticleMapper mapper;

    @BeforeEach
    public void setUp() {
        mapper = Mappers.getMapper(ArticleMapper.class);
    }

    @Test
    void articleToArticleDTO() {
        Article article = getArticle();
        ArticleDTO actual = mapper.articleToArticleDTO(article);

        assertEquals(article.getAuthor(), actual.getAuthor());
        assertEquals(article.getContent(), actual.getContent());
    }

    @Test
    void articlesToArticleDTOs() {
        List<Article> articles = new ArrayList<>();
        articles.add(getArticle());
        articles.add(getArticle());
        articles.add(getArticle());

        List<ArticleDTO> articleDTOs = mapper.articlesToArticleDTOs(articles);

        assertThat(articleDTOs).isNotEmpty().size().isEqualTo(3);
        assertThat(articleDTOs.get(0).getContent()).isEqualTo(articles.get(0).getContent());
    }

    @Test
    void articleDTOtoArticle() {
        ArticleDTO articleDTO = getArticleDTO();
        Article actual = mapper.articleDTOtoArticle(articleDTO);

        assertEquals(articleDTO.getAuthor(), actual.getAuthor());
        assertEquals(articleDTO.getContent(), actual.getContent());
    }

    private static Article getArticle() {
        return Article.builder().id(ARTICLE_ID)
                .author("Author").content("Content").build();
    }

    private static ArticleDTO getArticleDTO() {
        return ArticleDTO.builder().id(ARTICLE_ID)
                .author("Author").content("Content").build();
    }
}
