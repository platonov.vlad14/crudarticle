package com.article.storage.config;


import com.article.storage.mapper.ArticleMapper;
import com.article.storage.repository.ArticleRepository;
import com.article.storage.service.config.ArticleUpdateHandlerConfig;
import com.article.storage.service.impl.ArticleServiceImpl;
import com.article.storage.service.impl.handler.ArticleUpdateHandler;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Configuration
//@ComponentScan(basePackageClasses = { ArticleMapper.class })
@Profile("testConfig")
@Import(ArticleUpdateHandlerConfig.class)
public class ArticleServiceTestConfig {
    @Bean
    ArticleServiceImpl articleService(ArticleMapper mapper, ArticleRepository repository,
                                      @Qualifier("compositeArticleUpdateHandler") ArticleUpdateHandler updateHandler){
        return new ArticleServiceImpl(repository,mapper, updateHandler);
    }

    @Bean
    ArticleMapper articleMapper(){
        return Mappers.getMapper(ArticleMapper.class);
    }
}
