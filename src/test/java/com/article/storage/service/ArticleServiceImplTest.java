package com.article.storage.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.article.storage.config.ArticleServiceTestConfig;
import com.article.storage.dto.ArticleDTO;
import com.article.storage.mapper.ArticleMapper;
import com.article.storage.model.Article;
import com.article.storage.repository.ArticleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ArticleServiceTestConfig.class})
@ActiveProfiles("testConfig")
class ArticleServiceImplTest {
    private static final Long ARTICLE_ID = 1L;

    @Autowired
    private ArticleService service;

    @Autowired
    private ArticleMapper mapper;

    @MockBean
    private ArticleRepository repository;

    @Test
    void shouldReturnAllArticle() {
        List<Article> articles = new ArrayList<>();

        articles.add(Article.builder().id(1L).author("Author 1").content("Content 1").createdOn(LocalDateTime.now())
                .commercialPaymentsSum(BigDecimal.valueOf(100L)).comments("comments 1").build());
        articles.add(Article.builder().id(2L).author("Author 2").content("Content 2").createdOn(LocalDateTime.now())
                .commercialPaymentsSum(BigDecimal.valueOf(100L)).comments("comments 2").build());
        articles.add(Article.builder().id(3L).author("Author 3").content("Content 3").createdOn(LocalDateTime.now())
                .commercialPaymentsSum(BigDecimal.valueOf(100L)).comments("comments 3").build());

        List<ArticleDTO> articleDTOs = mapper.articlesToArticleDTOs(articles);

        when(repository.findAll()).thenReturn(articles);

        List<ArticleDTO> actual =  service.getAllArticle();

        assertThat(actual.size()).isEqualTo(3);
        assertThat(actual).isEqualTo(articleDTOs);
    }

    @Test
    void shouldCreateArticle() {
        Article article = getArticle();
        ArticleDTO expected = mapper.articleToArticleDTO(article);

        when(repository.save(any())).thenAnswer(returnsFirstArg());

        ArticleDTO actual = service.createArticle(expected);

        assertEquals(expected.getId(),actual.getId());
        assertEquals(expected.getAuthor(),actual.getAuthor());
        assertEquals(expected.getContent(),actual.getContent());
    }
//
    @Test
    void shouldReturnArticleById() {
        Article article = getArticle();
        ArticleDTO expected = mapper.articleToArticleDTO(article);

        when(repository.findById(ARTICLE_ID)).thenReturn(Optional.of(article));

        ArticleDTO actual = service.getArticleById(ARTICLE_ID);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void shouldUpdateArticle() {
        Article article = getArticle();
        ArticleDTO expected = ArticleDTO.builder().id(ARTICLE_ID)
                .author("Updated Author").content("Updated Content").build();

        when(repository.findById(ARTICLE_ID)).thenReturn(Optional.of(article));
        when(repository.save(article)).thenAnswer(returnsFirstArg());

        ArticleDTO actual = service.updateArticle(expected, ARTICLE_ID);

        assertThat(actual).isEqualTo(expected);

        verify(repository,times(1)).findById(any());
        verify(repository,times(1)).save(any());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void shouldDeleteArticle() {
        when(repository.findById(ARTICLE_ID)).thenReturn(Optional.of(new Article()));

        service.deleteArticle(ARTICLE_ID);

        verify(repository, times(1)).deleteById(ARTICLE_ID);
    }

    private static Article getArticle() {
        return Article.builder().id(ARTICLE_ID)
                .author("Author").content("Content").build();
    }
}